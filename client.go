package s3client

import "go.uber.org/zap"

type Client struct {
	RequestId  string
	Addr       string      // s3中间件请求地址
	ProName    string      // 操作品牌名称
	Region     string      // 服务所在区域信息
	ServerName string      // 请求服务名称
	Other      string      // 请求标记信息
	Encrypt    bool        // 是否加密请求数据模型
	Secret     string      // AES加密秘钥
	Logger     *zap.Logger // 操作注入日志驱动
}
