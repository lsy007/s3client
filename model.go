package s3client

type BucketResult struct {
	Name string `json:"name"` // 桶名称
	Date string `json:"date"` // 桶创建或最后一次更新属性的日期
}

type ListObjectInput struct {
	Delimiter string `json:"delimiter"` // 用于对键值分组的分隔字符串
	Marker    string `json:"marker"`    // 设置从指定键值开始获取
	MaxKeys   int64  `json:"max_keys"`  // 最大获取指定数量的数据，默认100
	Prefix    string `json:"prefix"`    // 查询键值前缀
}

type ObjectResult struct {
	Key          string `json:"key"`           // 键值
	Size         int64  `json:"size"`          // 大小，单位：字节
	Etag         string `json:"etag"`          // 标签
	LastModified string `json:"last_modified"` // 最后更新日期
	StorageClass string `json:"storage_class"` // 对象存储类型
}

type PutObjectInput struct {
	Key           string `json:"key"`            // 预签名对象键值
	Ttl           int64  `json:"ttl"`            // 预签名有效时长，默认 300 s
	Acl           string `json:"acl"`            // 对象权限范围，具体参考：https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl
	StorageClass  string `json:"storage_class"`  // 对象存储类型，可配合 ObjectResult 对存储对象进行分类管理
	Tagging       string `json:"tagging"`        // 存储标签
	Expires       int64  `json:"expires"`        // 对象过期时间戳（不会删除，只是在 s3 会标记过期）
	ContentType   string `json:"content_type"`   // 对象类型
	ContentLength int64  `json:"content_length"` // 对象大小，单位：字节
	ContentMd5    string `json:"content_md5"`    // 对象 MD5
}

type LifecycleRule struct {
	Id         string `json:"id"`         // 规则唯一ID
	Prefix     string `json:"prefix"`     // 应用规则的前缀
	Status     bool   `json:"status"`     // 规则启用状态, true-启用、false-禁用
	Expiration int64  `json:"expiration"` // 生命周期时长，单位：天
}
